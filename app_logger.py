import logging
import structlog

logger = logging.getLogger("net-auto")

stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.INFO)
stream_handler.setFormatter(logging.Formatter("%(levelname)s - %(message)s"))
logger.addHandler(stream_handler)


def logger_factory():
    logger.setLevel(logging.INFO)
    return logger


structlog.configure(
    logger_factory=logger_factory,
    processors=[
        structlog.processors.StackInfoRenderer(),
        structlog.dev.set_exc_info,
        structlog.processors.format_exc_info,
        structlog.processors.TimeStamper(fmt="%Y-%m-%d %H:%M.%S", utc=False),
        structlog.dev.ConsoleRenderer(colors=True),
    ]
)

LOG = structlog.get_logger()
