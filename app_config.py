from src.models import GoogleApiKey

api_key = GoogleApiKey.objects.filter(is_active=True).first()
if api_key is None:
    raise Exception('Please enter your active google api key')
DEVELOPER_KEY = api_key.key
YOUTUBE_API_SERVICE_NAME = 'youtube'
YOUTUBE_API_VERSION = 'v3'
KEY_WORD = 'cricket'
