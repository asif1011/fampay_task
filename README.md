# youtube-end

Back end code for the search the youtube latest videos

## Setup without docker

```sh
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt --no-cache
```
#### Setup Database
```
create .env file and set the following variables
DB_NAME
DB_PASS
DB_HOST
DB_PORT
```

#### Server Commands
```sh
python manage.py migrate
pyton manage.py createsuperuser
python manage.py runserver
python manage.py runworkers
```
##### Django-workers
See for more details of latest django workers  
[https://pypi.org/project/django-workers/](https://pypi.org/project/django-workers/)

## Setup with docker
1. Run the docker-compose up command from the top level directory for your project.
```
docker compose up --build
```

2. List running containers.
```sh
docker ps
```
3. Run background task
```sh
docker-compose run web manage.py runworkers
```

4. For a more elegant shutdown, switch to a different shell
```sh
docker-compose down
```