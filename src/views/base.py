from rest_framework import viewsets
from rest_framework import mixins
from rest_framework.pagination import PageNumberPagination


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 1000


class CreateListRetrieveUpdateDeleteViewSet(mixins.CreateModelMixin,
                                            mixins.ListModelMixin,
                                            mixins.RetrieveModelMixin,
                                            mixins.UpdateModelMixin,
                                            mixins.DestroyModelMixin,
                                            viewsets.GenericViewSet):
    pagination_class = StandardResultsSetPagination
