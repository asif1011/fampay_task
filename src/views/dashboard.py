from rest_framework.views import APIView
from rest_framework.response import Response
from django.db.models import Q
from src.models import YoutubeData
from .base import CreateListRetrieveUpdateDeleteViewSet
from src.serializers import YoutubeDataSerializer


class IndexView(APIView):
    permission_classes = ()

    @classmethod
    def get(cls, request):
        return Response("Hello from Asif!")


class YoutubeDataView(CreateListRetrieveUpdateDeleteViewSet):
    queryset = YoutubeData.objects.all() # noqa
    serializer_class = YoutubeDataSerializer
    permission_classes = ()

    def list(self, request, *args, **kwargs):
        query = request.GET.get('q', None)
        if query is not None:
            queryset = self.get_queryset()
            query_list = query.split()
            for q in query_list:
                q = q.strip()
                self.queryset = queryset.filter(
                    Q(title__icontains=q) |
                    Q(description__icontains=q)
                ).distinct()
        return super(YoutubeDataView, self).list(request, *args, **kwargs)
