from datetime import datetime
from googleapiclient.discovery import build # noqa
from googleapiclient.errors import HttpError # noqa
from app_logger import LOG
from app_config import YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION, DEVELOPER_KEY


def youtube_search(q, max_results=25, order='date', token=None, location=None, location_radius=None):
    response = {'next_token': None, 'items': []}

    try:
        youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION, developerKey=DEVELOPER_KEY)
        search_response = youtube.search().list(
            q=q,
            type='video',
            pageToken=token,
            order=order,
            part='id,snippet',
            maxResults=max_results,
            location=location,
            locationRadius=location_radius

        ).execute()
        response['items'] = [dict(keyword=q, video_id=item['id']['videoId'], title=item['snippet']['title'],
                                  description=item['snippet']['description'],
                                  published_at=datetime.strptime(item['snippet']['publishedAt'], '%Y-%m-%dT%H:%M:%SZ'),
                                  thumbnails=item['snippet']['thumbnails']
                                  ) for item in search_response.get('items', [])]
        response['next_token'] = search_response.get('nextPageToken', 'last_page')
    except HttpError as exc:
        LOG.error('An HTTP error occurred', status=exc.resp.status, content=exc.content)
        raise exc
    except Exception as exc:
        LOG.error('Error while fetching the search results', error=exc.__str__())
        raise exc

    return response
