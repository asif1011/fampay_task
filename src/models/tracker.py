# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils.translation import gettext_lazy as _
from .base import BaseModel


class Tracker(BaseModel):
    keyword = models.CharField(_('keyword'), max_length=120)
    next_token = models.CharField(_('next_token'), max_length=10, null=True)
    total_fetched_count = models.IntegerField(default=0)
    is_completed = models.BooleanField(default=False)

    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return self.keyword
