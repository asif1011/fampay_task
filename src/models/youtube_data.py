# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils.translation import gettext_lazy as _
from .base import BaseModel


class YoutubeData(BaseModel):
    keyword = models.CharField(_('keyword'), max_length=120)
    title = models.CharField(_('title'), max_length=255)
    description = models.TextField(_('description'))
    video_id = models.CharField(_('video id'), max_length=60)
    thumbnails = models.JSONField(null=True)
    published_at = models.DateTimeField()

    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return self.title
