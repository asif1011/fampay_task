# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils.translation import gettext_lazy as _
from .base import BaseModel


class GoogleApiKey(BaseModel):
    key = models.CharField(_('key'), max_length=120)
    is_active = models.BooleanField(default=False)

    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return self.key
