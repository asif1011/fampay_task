from django.urls import path
from src.views import YoutubeDataView

urlpatterns = [
    path('youtube-search', YoutubeDataView.as_view({'get': 'list'}))
]
