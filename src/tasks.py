import os
from workers import task
from src.models import Tracker, YoutubeData
from src.utils.youtube_api import youtube_search
from app_config import KEY_WORD


@task(schedule=10)
def search_and_add_videos():
    tracker, created = Tracker.objects.get_or_create(keyword=KEY_WORD) # noqa
    # for the first time next_token will be None
    if not tracker.is_completed:
        response_data = youtube_search(q=KEY_WORD, token=tracker.next_token)
        bulk_data = [YoutubeData(**data) for data in response_data['items']]
        YoutubeData.objects.bulk_create(bulk_data) # noqa
        tracker.next_token = response_data['next_token']
        tracker.total_fetched_count = YoutubeData.objects.filter(keyword=keyword).count() # noqa
        if response_data['next_token'] == 'last_page':
            tracker.is_completed = True
        tracker.save()
