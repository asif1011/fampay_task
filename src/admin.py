from django.contrib import admin
from src.models import (YoutubeData, GoogleApiKey, Tracker)


@admin.register(YoutubeData)
class YoutubeDataAdmin(admin.ModelAdmin):
    search_fields = ['keyword', 'title']
    list_display = ('video_id', 'keyword', 'title', 'published_at')


@admin.register(GoogleApiKey)
class GoogleApiKeyAdmin(admin.ModelAdmin):
    list_display = ('key', 'is_active')


@admin.register(Tracker)
class TrackerAdmin(admin.ModelAdmin):
    list_display = ('keyword', 'is_completed')
